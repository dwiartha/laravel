<?php

namespace App\Models;

class Post 
{
    private static $blog_posts =[
        [
            "title" => "Post Pertama",
            "slug" => "judul-post-pertama",
            "author" => "Sandy",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor neque, sequi architecto libero quo voluptatum veritatis ipsum eius tenetur fugiat magni ex reprehenderit rerum eum voluptas, placeat, iste ducimus. Obcaecati repellat quis eos est temporibus! Consectetur cum autem, vero maiores illum nam dolor deleniti tenetur minus ratione optio placeat magnam consequuntur et, sit minima, reiciendis architecto laudantium officiis ipsa nulla fuga velit eos! Corporis amet, deleniti numquam molestias placeat, ipsa reiciendis consectetur, maxime sed sit corrupti est voluptatibus laboriosam eveniet."
        ],
        [
            "title" => "Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "Dedy",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro laboriosam repudiandae adipisci recusandae velit culpa eum maxime? Assumenda sequi, necessitatibus suscipit et laboriosam reprehenderit veniam. Iusto fugiat corrupti, ullam, reiciendis sed nulla doloremque odio sunt voluptatum recusandae voluptas laborum inventore ut officiis illum ratione vitae dignissimos maiores id sint consequuntur explicabo! Repellendus dignissimos consectetur incidunt perferendis ex autem rerum, deleniti officia cupiditate! Repellat tempore minima dolore unde necessitatibus beatae, alias molestias iusto praesentium ducimus sapiente eos obcaecati officia et animi quos, reprehenderit porro quam exercitationem! Tempore maxime veritatis explicabo voluptas ad voluptates tempora, voluptatem quis sapiente nostrum architecto harum ab, eaque, ducimus totam quo dicta aliquid porro aspernatur a voluptatibus hic molestias. Maiores architecto quod, totam soluta vero ea ipsum est minima dolorem eveniet quisquam eaque magnam facilis error debitis quo atque nisi consequuntur impedit optio molestias obcaecati temporibus? Culpa qui suscipit animi voluptatum magni quam nulla ex iure, dolorem delectus aut error repellendus cum ullam esse quidem excepturi consequatur ipsa impedit! Molestiae id, suscipit fuga ab ex nemo nihil. Doloribus quaerat consequuntur exercitationem officiis. Voluptate et, quia mollitia similique ea ad, itaque error facere voluptates nemo ipsa quam reiciendis praesentium accusamus dolorum reprehenderit cum eum? Magni necessitatibus temporibus omnis."
        ],
    ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $posts = static::all();
        // $post = [];
        // foreach($posts as $p){
        //     if($p["slug"] === $slug) {
        //         $post = $p;
        //     }
        // }

        return $posts->firstWhere('slug', $slug);
    }
}

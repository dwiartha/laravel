<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        
        User::create([
                'name' => 'Sandy',
                'username' => 'sandy',
                'email' => 'Kadeksandy@dmail.com',
                'password' => bcrypt('12345')
            ]);
        // User::create([
            //     'name' => 'dedy',
        //     'email' => 'dedywwanditya@dmail.com',
        //     'password' => bcrypt('12345')
        // ]);

        User::factory(3)->create();

        Category::create([
            'name'  => 'Web-Programing',
            'slug'  => 'web-programing'

        ]);
        Category::create([
            'name'  => 'Web-Design',
            'slug'  => 'web-design'

        ]);
        Category::create([
            'name'  => 'Personal',
            'slug'  => 'personal'
        ]);
        
        Post::factory(20)->create();

        // Post::create([
        //     'title'  => 'Judul Pertama',
        //     'slug'  => 'judul-pertama',
        //     'excerpt'  => 'ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body'  => '<p>ipsum dolor sit amet consectetur adipisicing elit. Consequatur, in. Assumenda iusto inventore repellendus tempora ex debitis voluptatum quibusdam hic? Sed, numquam eum architecto doloribus harum odio id molestiae, cupiditate est ut minus corporis maxime nisi quia quam. Vitae dolor quos placeat, numquam molestiae accusantium dolorum deleniti. Repellendus repudiandae quae exercitationem ab eius voluptatem dolore iusto.</p><p>Perspiciatis itaque aliquid illo ipsam temporibus saepe, quaerat quibusdam, debitis explicabo non natus similique sunt, ab aperiam quo culpa optio! Nobis rerum</p><p>culpa explicabo cum ipsam ad corrupti temporibus, adipisci tempora dolorum in quisquam repellat aut ea nulla harum aspernatur perferendis quod? Et voluptatibus beatae officiis sit libero rem consectetur culpa? Sit perspiciatis doloremque in corrupti vel incidunt eos aliquid magni doloribus ex tempora pariatur accusamus quis culpa ratione harum blanditiis, iure ducimus fugit? Minima quod nobis cupiditate voluptatum quia molestiae doloribus, quos fugiat. Ea esse aliquam repellendus omnis neque at tempora aspernatur provident.</p>',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title'  => 'Judul Kedua',
        //     'slug'  => 'judul-ke-dua',
        //     'excerpt'  => 'ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body'  => '<p>ipsum dolor sit amet consectetur adipisicing elit. Consequatur, in. Assumenda iusto inventore repellendus tempora ex debitis voluptatum quibusdam hic? Sed, numquam eum architecto doloribus harum odio id molestiae, cupiditate est ut minus corporis maxime nisi quia quam. Vitae dolor quos placeat, numquam molestiae accusantium dolorum deleniti. Repellendus repudiandae quae exercitationem ab eius voluptatem dolore iusto.</p><p>Perspiciatis itaque aliquid illo ipsam temporibus saepe, quaerat quibusdam, debitis explicabo non natus similique sunt, ab aperiam quo culpa optio! Nobis rerum</p><p>culpa explicabo cum ipsam ad corrupti temporibus, adipisci tempora dolorum in quisquam repellat aut ea nulla harum aspernatur perferendis quod? Et voluptatibus beatae officiis sit libero rem consectetur culpa? Sit perspiciatis doloremque in corrupti vel incidunt eos aliquid magni doloribus ex tempora pariatur accusamus quis culpa ratione harum blanditiis, iure ducimus fugit? Minima quod nobis cupiditate voluptatum quia molestiae doloribus, quos fugiat. Ea esse aliquam repellendus omnis neque at tempora aspernatur provident.</p>',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title'  => 'Judul Ketiga',
        //     'slug'  => 'judul-ke-tiga',
        //     'excerpt'  => 'ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body'  => '<p>ipsum dolor sit amet consectetur adipisicing elit. Consequatur, in. Assumenda iusto inventore repellendus tempora ex debitis voluptatum quibusdam hic? Sed, numquam eum architecto doloribus harum odio id molestiae, cupiditate est ut minus corporis maxime nisi quia quam. Vitae dolor quos placeat, numquam molestiae accusantium dolorum deleniti. Repellendus repudiandae quae exercitationem ab eius voluptatem dolore iusto.</p><p>Perspiciatis itaque aliquid illo ipsam temporibus saepe, quaerat quibusdam, debitis explicabo non natus similique sunt, ab aperiam quo culpa optio! Nobis rerum</p><p>culpa explicabo cum ipsam ad corrupti temporibus, adipisci tempora dolorum in quisquam repellat aut ea nulla harum aspernatur perferendis quod? Et voluptatibus beatae officiis sit libero rem consectetur culpa? Sit perspiciatis doloremque in corrupti vel incidunt eos aliquid magni doloribus ex tempora pariatur accusamus quis culpa ratione harum blanditiis, iure ducimus fugit? Minima quod nobis cupiditate voluptatum quia molestiae doloribus, quos fugiat. Ea esse aliquam repellendus omnis neque at tempora aspernatur provident.</p>',
        //     'category_id' => 2,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title'  => 'Judul Keempat',
        //     'slug'  => 'judul-ke-empat',
        //     'excerpt'  => 'ipsum dolor sit amet consectetur adipisicing elit.',
        //     'body'  => '<p>ipsum dolor sit amet consectetur adipisicing elit. Consequatur, in. Assumenda iusto inventore repellendus tempora ex debitis voluptatum quibusdam hic? Sed, numquam eum architecto doloribus harum odio id molestiae, cupiditate est ut minus corporis maxime nisi quia quam. Vitae dolor quos placeat, numquam molestiae accusantium dolorum deleniti. Repellendus repudiandae quae exercitationem ab eius voluptatem dolore iusto.</p><p>Perspiciatis itaque aliquid illo ipsam temporibus saepe, quaerat quibusdam, debitis explicabo non natus similique sunt, ab aperiam quo culpa optio! Nobis rerum</p><p>culpa explicabo cum ipsam ad corrupti temporibus, adipisci tempora dolorum in quisquam repellat aut ea nulla harum aspernatur perferendis quod? Et voluptatibus beatae officiis sit libero rem consectetur culpa? Sit perspiciatis doloremque in corrupti vel incidunt eos aliquid magni doloribus ex tempora pariatur accusamus quis culpa ratione harum blanditiis, iure ducimus fugit? Minima quod nobis cupiditate voluptatum quia molestiae doloribus, quos fugiat. Ea esse aliquam repellendus omnis neque at tempora aspernatur provident.</p>',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);
    }
}

@extends('layouts.main')

@section('container')
<h1>Halaman About</h1>
<table border="1">
    <tr>
        <td><img src="img/{{ $image }}" alt="" width="200" class="img-thumbnail rounded-circle"></td>
        <td><h3> {{ $name }} </h3></td>
        <td><h3> {{ $email }} </h3></td>
    </tr>
</table>
@endsection
